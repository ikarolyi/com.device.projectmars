package com.device.projectmars;

//findViewById(R.id.bottomright).setOnDragListener(new MyDragListener());

import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

class DragListener implements View.OnDragListener {
    /*Drawable enterShape = getResources().getDrawable(
            R.drawable.ic_launcher_background);
    Drawable normalShape = getResources().getDrawable(R.mipmap.ic_launcher);*/

    @Override
    public boolean onDrag(View v, DragEvent event) {
        int action = event.getAction();
        View view = (View) event.getLocalState();
        ViewGroup owner = (ViewGroup) view.getParent();
        LinearLayout container = (LinearLayout) v;

        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
                // do nothing
                break;
            case DragEvent.ACTION_DRAG_ENTERED:
                //v.setBackgroundDrawable(enterShape);
                System.out.println("entered");
                break;
            case DragEvent.ACTION_DRAG_EXITED:
                //v.setBackgroundDrawable(normalShape);
                System.out.println("exited");
                owner.removeView(view);
                container.addView(view);
                view.setVisibility(View.VISIBLE);
                break;
            case DragEvent.ACTION_DROP:
                // Dropped, reassign View to ViewGroup
                owner.removeView(view);
                container.addView(view);
                view.setVisibility(View.VISIBLE);
                break;
            case DragEvent.ACTION_DRAG_ENDED:
                System.out.println("ended");
                //v.setBackgroundDrawable(normalShape);
            default:
                break;
        }
        return true;


    }
}
