package com.device.projectmars;

// Assign the touch listener to your view which you want to move

import android.content.ClipData;
import android.view.MotionEvent;
import android.view.View;
//findViewById(R.id.myimage1).setOnTouchListener(new MyTouchListener());

// This defines your touch listener
public final class TouchListener implements View.OnTouchListener {
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
                    view);
            view.startDrag(data, shadowBuilder, view, 0);
            view.setVisibility(View.INVISIBLE);
            return true;
        } else {
            return false;
        }
    }
}
