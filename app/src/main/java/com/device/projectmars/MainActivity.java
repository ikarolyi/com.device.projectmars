package com.device.projectmars;
/**
 * By The Developer Ice
 * &copy;2020 The DevIce, DevIce Programmer Team(DIPT)
 * Git: https://gitlab.com/ikarolyi/com.device.projectmars (public project)
 * DON'T COPY THIS CODE!!!
 **/

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

public class MainActivity extends AppCompatActivity {

    private int progressStatus = 0;
    private Handler handler = new Handler();
    public int quest_01_monsters_counter = 5;
    public int quest_02_taps_counter = 0;
    public int quest_06_num_01 = 0;
    public int quest_06_num_02 = 0;
    public int quest_06_num_03 = 0;
    public int quest_09_eat_progress = 0;
    public boolean quest_04_completed = true;
    private final static int RC_SIGN_IN = 2;

    // Configure sign-in to request the user's ID, email address, and basic
    // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
    public GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
            .requestEmail()
            .build();

    public boolean sounds(){
        SharedPreferences prfs = getSharedPreferences("AUTHENTICATION_FILE_NAME", Context.MODE_PRIVATE);
        return prfs.getBoolean("allowSoundEffects", true);
    }

    //Change UI according to user data.
    public void updateUI(GoogleSignInAccount account){

        if(account != null){
            Toast.makeText(this,"U Signed In successfully",Toast.LENGTH_LONG).show();
            startActivity(new Intent(this, MainActivity.class));
        }else {
            Toast.makeText(this,"U Didnt signed in",Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("TAG","signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }else{
            showSystemUI();
        }
    }
    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    // Shows the system bars by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    public void signIn(View v) {

        System.out.println("nnnnnnoooooooooowwwww");
        // Build a GoogleSignInClient with the options specified by gso.
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        GoogleApiAvailability instance = GoogleApiAvailability.getInstance();
        System.out.println("\n\n\n\nGoogle play services: " + instance.toString()/*.isGooglePlayServicesAvailable(this)*/);
        System.out.println(instance.isGooglePlayServicesAvailable(getApplicationContext()));
        if (instance.isGooglePlayServicesAvailable(getApplicationContext()) != ConnectionResult.SUCCESS){
            instance.getErrorDialog(new Activity(), instance.isGooglePlayServicesAvailable(getApplicationContext()), ConnectionResult.SUCCESS);
        }else {
            signIn(null);
        }
        System.out.println("ITT ^^ ITT");
        Toast.makeText(getApplicationContext(), instance.toString(), Toast.LENGTH_LONG).show();
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);
        ImageView device = findViewById(R.id.device);

        onWindowFocusChanged(true);

        AlphaAnimation animation1 = new AlphaAnimation(1.0f, 0.0f);
        animation1.setDuration(2000);
        animation1.setStartOffset(1000);
        animation1.setFillAfter(true);
        device.startAnimation(animation1);
        View decorView = getWindow().getDecorView();
        System.out.println(decorView.getSystemUiVisibility());

        Handler start = new Handler();
        start.postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);
                setContentView(R.layout.main);
                ProgressBar       location = findViewById(R.id.progressBar);
                SharedPreferences prfs     = getSharedPreferences("AUTHENTICATION_FILE_NAME", Context.MODE_PRIVATE);
                ImageView         fire     = findViewById(R.id.imageView4);

                int    locint = prfs.getInt("location", 0);
                Button shopb  = findViewById(R.id.shopb);
                shopb.setEnabled(false);
                Button launnchb = findViewById(R.id.launchb);
                launnchb.setEnabled(false);
                Button getquestb = findViewById(R.id.qetquestb);
                getquestb.setEnabled(false);
                Button explore = findViewById(R.id.explore);

                location.setProgress(locint);
                if (locint == 0) {
                    shopb.setEnabled(true);
                    launnchb.setEnabled(true);
                    fire.setVisibility(View.INVISIBLE);
                }else if (locint > 100) {
                    fire.setVisibility(View.INVISIBLE);
                    launnchb.setEnabled(true);
                    explore.setEnabled(true);
                } else {
                    fire.setVisibility(View.VISIBLE);
                    shopb.setEnabled(false);
                    launnchb.setEnabled(false);
                    getquestb.setEnabled(true);
                }
            }
        }, 3500);
    }

    public void launch(View view){
        SharedPreferences prfs = getSharedPreferences("AUTHENTICATION_FILE_NAME", Context.MODE_PRIVATE);
        int locint = prfs.getInt("location", 0);
        SharedPreferences.Editor editor = prfs.edit();
        ProgressBar location = findViewById(R.id.progressBar);
        ImageView fire = findViewById(R.id.imageView4);
        Button getquestb = findViewById(R.id.qetquestb);
        Button launnchb = findViewById(R.id.launchb);
        Button shopb = findViewById(R.id.shopb);

        if (locint == 0){
            editor.putInt("location", 1);
            location.setProgress(1);
            shopb.setEnabled(false);
            launnchb.setEnabled(false);
            getquestb.setEnabled(true);
            fire.setVisibility(View.VISIBLE);
            quest(null);
        }else {
            editor.putInt("location", 0);
            shopb.setEnabled(true);
            getquestb.setEnabled(false);
            location.setProgress(0);
            fire.setVisibility(View.INVISIBLE);
        }
        editor.apply();
    }

    public void quest(View view){
        double adrandom = Math.random()*10;
        int ad = (int)adrandom;
        long adwait = 0;
        if (ad == 0){
            adwait = 3000;
            setContentView(R.layout.ad);
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                double mrandom = Math.random()*10;//10
                int random = (int)mrandom;
                System.out.println(random);
                if(random == 0){
                    setContentView(R.layout.quest_01);
                    quest_01_monsters_counter = 5;
                }else if(random == 1){
                    setContentView(R.layout.quest_02);
                    quest_02_taps_counter = 0;
                }else if(random == 2){
                    setContentView(R.layout.quest_03);
                }else if(random == 3){
                    setContentView(R.layout.quest_04);
                    quest_04_completed = false;
                    quest_04_color_action();
                }else if (random == 4){
                    setContentView(R.layout.quest_05);
                }else if (random == 5){
                    double quest_06_num_01_long = Math.random()*11;
                    double quest_06_num_02_long = Math.random()*11;
                    double quest_06_num_03_long = Math.random()*11;
                    quest_06_num_01 = (int)quest_06_num_01_long;
                    quest_06_num_02 = (int)quest_06_num_02_long;
                    quest_06_num_03 = (int)quest_06_num_03_long;
                    setContentView(R.layout.quest_06);

                    TextView textView = findViewById(R.id.textView15);
                    textView.setText(quest_06_num_01 + "+" + quest_06_num_02 + "-" + quest_06_num_03 + "=");
                }else if (random==6) {
                    setContentView(R.layout.quest_07);

                    //findViewById(R.id.myimage1).setOnTouchListener(new TouchListener());
                    //findViewById(R.id.myimage2).setOnTouchListener(new TouchListener());
                    findViewById(R.id.myimage3).setOnTouchListener(new TouchListener());
                    findViewById(R.id.myimage4).setOnTouchListener(new TouchListener());
                    findViewById(R.id.target).setOnDragListener(new DragListener());
                    //findViewById(R.id.topright).setOnDragListener(new DragListener());
                    findViewById(R.id.bottomleft).setOnDragListener(new DragListener());
                    //findViewById(R.id.bottomright).setOnDragListener(new DragListener());
                }else if(random == 7){
                    setContentView(R.layout.quest_08);

                    findViewById(R.id.quest_08_rocket).setOnTouchListener(new TouchListener());

                    findViewById(R.id.quest_08_mars).setOnDragListener(new DragListener());
                    findViewById(R.id.quest_08_earth).setOnDragListener(new DragListener());
                }else if(random == 8){
                    setContentView(R.layout.quest_09);
                    quest_09_eat_progress = 0;
                }else if(random == 9){
                    setContentView(R.layout.quest_10);
                    quest_10_hammer_rotation = 0;
                }
            }
        }, adwait);

    }

    public void quest_01_aliens_action(View view){
        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.laser);
        if (!playSound(mediaPlayer)) {
            mediaPlayer.release();
        }
        view.setVisibility(View.GONE);
        quest_01_monsters_counter--;
        if (quest_01_monsters_counter == 0){
            questComplete();
        }
    }
    public void quest_02_level_action(View view){
        MediaPlayer quest_02_media_player = MediaPlayer.create(getApplicationContext(), R.raw.game_click);
        if (!playSound(quest_02_media_player)) {
            quest_02_media_player.release();
        }
        ProgressBar progressBar = findViewById(R.id.progressBar2);
        quest_02_taps_counter++;
        quest_02_taps_counter++;
        quest_02_taps_counter++;
        progressBar.setProgress(quest_02_taps_counter);
        if (quest_02_taps_counter >= 100){
            questComplete();
        }

    }

    public void quest_03_login_action(View view){
        EditText username = findViewById(R.id.quest_03_username_input);
        EditText email    = findViewById(R.id.quest_03_email_input);
        EditText password = findViewById(R.id.quest_03_password_input);
        CheckBox robot    = findViewById(R.id.switch5);

        System.out.println(username.getText().length() >= 1&&email.getText().length() >= 1&&password.getText().length() >= 1&&robot.isChecked() == true);
        if(username.getText().length() >= 1&&email.getText().length() >= 1&&password.getText().length() >= 1&&robot.isChecked() == true){
            questComplete();
        }
    }
    public void quest_04_color_action(){
        ImageView a = findViewById(R.id.quest_04_dot_01);
        ImageView b = findViewById(R.id.quest_04_dot_02);
        ImageView c = findViewById(R.id.quest_04_dot_03);
        ImageView d = findViewById(R.id.quest_04_dot_04);
        ImageView e = findViewById(R.id.quest_04_dot_05);
        ImageView f = findViewById(R.id.quest_04_dot_06);
        ImageView g = findViewById(R.id.quest_04_dot_07);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                a.setColorFilter(Color.parseColor("#ff3d00"));
                b.setColorFilter(Color.parseColor("#7ef542"));
                c.setColorFilter(Color.parseColor("#7ef542"));
                d.setColorFilter(Color.parseColor("#7ef542"));
                e.setColorFilter(Color.parseColor("#ff3d00"));
                f.setColorFilter(Color.parseColor("#7ef542"));
                g.setColorFilter(Color.parseColor("#ff3d00"));
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        a.setColorFilter(Color.parseColor("#4287f5"));
                        b.setColorFilter(Color.parseColor("#ff3d00"));
                        c.setColorFilter(Color.parseColor("#4287f5"));
                        d.setColorFilter(Color.parseColor("#ff3d00"));
                        e.setColorFilter(Color.parseColor("#4287f5"));
                        f.setColorFilter(Color.parseColor("#ff3d00"));
                        g.setColorFilter(Color.parseColor("#4287f5"));
                        if (!quest_04_completed){
                            quest_04_color_action();
                        }
                    }
                }, 250);
            }
        }, 250);
    }

    public void quest_04_submit_action(View view){
        EditText dots = findViewById(R.id.quest_04_dots_input);

        if(dots.getText().toString().equals("7")){
            quest_04_completed = true;
            questComplete();
        }
    }

    public void questComplete(){
        onWindowFocusChanged(true);
        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.questcomplete);
        if (!playSound(mediaPlayer)) {
        mediaPlayer.release();
        }
        Toast.makeText(getApplicationContext(), "Quest completed!", Toast.LENGTH_SHORT).show();
        setContentView(R.layout.main);
        SharedPreferences prfs = getSharedPreferences("AUTHENTICATION_FILE_NAME", Context.MODE_PRIVATE);
        int locint = prfs.getInt("location", 0);
        SharedPreferences.Editor editor = prfs.edit();
        editor.putInt("location", locint+1);
        editor.apply();
        locint = prfs.getInt("location", 0);
        ProgressBar progressBar = findViewById(R.id.progressBar);
        progressBar.setProgress(locint);

        Button getquestb = findViewById(R.id.qetquestb);
        Button launnchb = findViewById(R.id.launchb);
        Button shopb = findViewById(R.id.shopb);
        System.out.println(locint);
        if (locint < 100) {
            shopb.setEnabled(false);
            launnchb.setEnabled(false);
            getquestb.setEnabled(true);
        }else {
            shopb.setEnabled(false);
            launnchb.setEnabled(true);
            getquestb.setEnabled(false);
        }
    }

    public void quest_05_submit_action(View view) {
        EditText triangles = findViewById(R.id.quest_05_triangles_input);
        if (triangles.getText().toString().equals("28")){
            questComplete();
        }else if(triangles.getText().toString().equals("27")){
            Toast.makeText(getApplicationContext(), "Nope", Toast.LENGTH_SHORT).show();
        }
        triangles.setText("");
    }

    public void quest_06_submit_button(View view) {
        int equalation = quest_06_num_01+quest_06_num_02-quest_06_num_03;
        EditText input = findViewById(R.id.quest_06_equalation_input);
        if (input.getText().length() != 0) {
            if (Integer.parseInt(input.getText().toString()) == equalation) {
                questComplete();
            }
        }
    }

    public void quest_07_upload_action(View view) {
        LinearLayout target = findViewById(R.id.target);

        if (target.getChildCount() == 3){
            findViewById(R.id.textView19).setVisibility(View.VISIBLE);
            quest_07_uploadbar();
        }
    }

    public void quest_07_uploadbar(){
        ProgressBar progressBar = findViewById(R.id.progressBar3);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setProgress(progressBar.getProgress() + 1);
                if (progressBar.getProgress() <= 99){
                    quest_07_uploadbar();
                }else {
                    TextView textView = findViewById(R.id.textView19);
                    textView.setText("Done uploading!");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            questComplete();
                        }
                    }, 500);
                }
            }
        }, 20);

    }
    public void quest_08_saveplan_action(View view) {
        LinearLayout mars = findViewById(R.id.quest_08_mars);
        if (mars.getChildCount() == 1){
            findViewById(R.id.imageView14).setEnabled(false);
            Toast.makeText(getApplicationContext(), "Saving plan...", Toast.LENGTH_SHORT).show();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Done!", Toast.LENGTH_SHORT).show();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            questComplete();
                        }
                    }, 500);
                }
            }, 1000);
        }
    }

    public void settings(View view) {
        setContentView(R.layout.settings);
        ConstraintLayout settings = findViewById(R.id.settingslayout);
        ProgressBar load = findViewById(R.id.settingsload);
        Switch allowSoundEffects = findViewById(R.id.setbgm);
        Button about = findViewById(R.id.about);
        Button repbug = findViewById(R.id.repbugb);
        SharedPreferences prfs = getSharedPreferences("AUTHENTICATION_FILE_NAME", Context.MODE_PRIVATE);
        boolean allowSoundEffectsState = prfs.getBoolean("allowSoundEffects", true);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                settings.setVisibility(View.VISIBLE);
                about.setVisibility(View.VISIBLE);
                repbug.setVisibility(View.VISIBLE);
                allowSoundEffects.setChecked(allowSoundEffectsState);
            }
        }, 500);
    }

    public void setsave(View view) {
        ConstraintLayout settings = findViewById(R.id.settingslayout);
        ProgressBar load = findViewById(R.id.settingsload);
        Switch allowSoundEffects = findViewById(R.id.setbgm);
        Button about = findViewById(R.id.about);

        SharedPreferences prfs = getSharedPreferences("AUTHENTICATION_FILE_NAME", Context.MODE_PRIVATE);
        Editor editor = prfs.edit();
        editor.putBoolean("allowSoundEffects", allowSoundEffects.isChecked());

        editor.apply();

        System.out.println(sounds());
        /*if (allowSoundEffects.isChecked() == true){
        }else if (allowSoundEffects.isChecked() == false){
        }*/
    }

    public void backhome(View view) {
        setContentView(R.layout.main);
        SharedPreferences prfs = getSharedPreferences("AUTHENTICATION_FILE_NAME", Context.MODE_PRIVATE);
        int locint = prfs.getInt("location", 0);
        ProgressBar progressBar = findViewById(R.id.progressBar);
        progressBar.setProgress(locint);
        Button getquestb = findViewById(R.id.qetquestb);
        Button launnchb = findViewById(R.id.launchb);
        Button shopb = findViewById(R.id.shopb);
        if (locint < 100) {
            shopb.setEnabled(false);
            launnchb.setEnabled(false);
            getquestb.setEnabled(true);
        }else {
            shopb.setEnabled(false);
            launnchb.setEnabled(true);
            getquestb.setEnabled(true);
        }
    }

    public void about(View view) {
        setContentView(R.layout.about);
    }

    public boolean playSound(MediaPlayer sound){
        boolean ok = true;
        sound.setOnErrorListener(new MediaPlayer.OnErrorListener(){
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                System.out.println("Error");
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                repbug(null);
                                //Yes button clicked
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setMessage("The game found an error while playing sound.\nDo you want report it as bug?\n(If you press no, the audio won't work)").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

                sound.reset();
                boolean ok = false;
                return true;
            }
        });
        if (sounds() == true&&!sound.isPlaying()){
            sound.start();
        }//else do nothing
        return ok;
    }



    public void repbug(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:?to=thedevicedevs@gmail.com&subject=Project%20Mars%20bug%20report&body=Bug%20description:%20...%0aWhat%20were%20you%20doing%20when%20the%20bug%20occured:%20...%0aPlease%20notify%20me%20when%20the%20bug%20has%20been%20found(yes/no):%20..."));
        startActivity(browserIntent);
    }

    public void quest_09_eat_action(View view) {
        MediaPlayer eat = MediaPlayer.create(getApplicationContext(), R.raw.eating);
        ImageView bread = findViewById(R.id.imageView11);
        eat.setVolume(49,49);
        playSound(eat);

        quest_09_eat_progress++;
        /*switch (quest_09_eat_progress){           //TODO: uncommentelni amikor az illusztrátor elkészül a feladatával
            case 1:
                bread.setImageResource(R.drawable.xy);
                break;
            case 2:
                bread.setImageResource(R.drawable.xy);
                break;
            case 3:
                bread.setImageResource(R.drawable.xy);
                break;
            case 4:
                bread.setImageResource(R.drawable.xy);
                break;
        }*/
        if (5 <= quest_09_eat_progress){
            bread.setVisibility(View.INVISIBLE);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    questComplete();
                }
            }, 500);
        }
    }

    public void quest_10_computer_action(View view) {
        findViewById(R.id.imageView13).setEnabled(false);
        view.setEnabled(false);
        findViewById(R.id.imageView15).setVisibility(View.VISIBLE);
        findViewById(R.id.progressBar4).setVisibility(View.VISIBLE);
        quest_10_load_computer();
    }

    public int quest_10_hammer_rotation = 0;
    public void quest_10_rotate_hammer(){
        ImageView hammer = findViewById(R.id.imageView13);
        ConstraintLayout constraintLayout = findViewById(R.id.quest_10_constraintlayout);
        System.out.println(quest_10_hammer_rotation);
        if (quest_10_hammer_rotation < 90){
            quest_10_hammer_rotation++;
            hammer.setRotation(hammer.getRotation() - 1);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    quest_10_rotate_hammer();
                }
            }, 10);
        }else if (hammer.getY() <= constraintLayout.getHeight()){
            quest_10_hammer_move();
        }else {
            Toast.makeText(getApplicationContext(), "Don't crash the computer!", Toast.LENGTH_SHORT).show();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setContentView(R.layout.quest_10);
                    quest_10_hammer_rotation = 0;
                }
            }, 500);

        }
    }
    public void quest_10_hammer_move(){
        ImageView hammer = findViewById(R.id.imageView13);
        hammer.setY(hammer.getY() + 10);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                quest_10_rotate_hammer();
            }
        }, 10);
    }

    public void quest_10_load_computer() {
        ProgressBar load = findViewById(R.id.progressBar4);
        load.setProgress(load.getProgress() + 1);
        if (load.getProgress() != 100){
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    quest_10_load_computer();
                }
            }, 5);
        }else{
            questComplete();
        }
    }

    public void quest_10_hammer_action(View view) {
        MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.hammer);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                playSound(mediaPlayer);
            }
        }, 1750);

        ImageView computer = findViewById(R.id.imageView12);
        computer.setEnabled(false);
        quest_10_rotate_hammer();
    }

    public void explore(View view) {
        setContentView(R.layout.mars);
    }
}